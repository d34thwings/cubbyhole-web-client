var cubbyHole = angular.module('cubbyHole', ['ngRoute']);

cubbyHole.config(['$routeProvider',
  function($routeProvider) {
	$routeProvider.
	  when('/login', {
		templateUrl: 'pages/login.html'
	  }).
	  when('/register', {
		templateUrl: 'pages/register.html'
	  }).
	  when('/reset', {
		templateUrl: 'pages/reset.html'
	  }).
	  otherwise({
		redirectTo: '/login'
	  });
}]);

cubbyHole.factory('APIService', function() {
	var api_url = "http://api-cubbyhole.rhcloud.com/";
	var access_token = "";
	return {
		getApiUrl: function (){
			return api_url;
		},
		getAccessToken: function (){
			return access_token;
		},
		setAccessToken: function (string){
			access_token = string;
		},
        authenticateUser: function(email, password, callback) {
        	$.ajax({
				type: "POST",
				url: APIService.getApiUrl()+"oauth/token",
				headers: {
					Authorization: "Basic ODM5MjM3OTQ0ODY2MDo5MzA1ZDYyMWYxOWI4OGM2MjllZmU4NTQxMGQxZWIwYg=="
				},
				data: {
					grant_type: "password",
					username: email,
					password: password
				}
			}).done(function(oauth){
				APIService.setAccessToken(oauth.access_token);
				callback(oauth);
			}).error(function(error) {
				callback(error);
			});;
        }
	};
});