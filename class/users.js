cubbyHole.controller('UserCtrl', ['$scope', "APIService", "Users", "$http", function ($scope, APIService, $http, Users) {
	$scope.user = {
		'lname' : "",
		'fname' : "",
		'email' : "",
		'password' : "",
		'plan' : 0
	};

	$scope.checkForm = function() {
		var errors = [];

		angular.forEach($scope.register.$error, function(error, index) {
			if(error.length > 0){
				angular.forEach(error, function(info){
					errors.push(info.$name+"_required");
				});
			}
		});

		if($scope.user.password !== $scope.confirmation && $.inArray("confirmation", errors) == false) {
			errors.push("confirmation_wrong");
		}
		else{
			changeInputIconColor("password");
			changeInputIconColor("confirmation");
		}

		if(!validateEmail($scope.user.email)){
			errors.push("email_wrong");
		}
		else {
			changeInputIconColor("email");
		}

		if($scope.user.lname != "" && $scope.register.lname.$error.minlength) {
			errors.push("lname_length");
		}
		else{
			changeInputIconColor("lname");
		}

		if($scope.user.fname != "" && $scope.register.fname.$error.minlength) {
			errors.push("fname_length");
		}
		else{
			changeInputIconColor("fname");
		}

		if(errors.length > 0) {
			console.log(errors);
			angular.forEach(errors, function(error) {
				var infos = error.split("_");
				changeInputIconColor(infos[0], "red");
				sendError(infos);
			});
		}
		else {
			console.log($scope.user);
		}
	};

	$scope.login = function() {
		console.log(Users.getUsers());

		// $http({
		// 	method: "POST", 
		// 	url: APIService.getApiUrl()+"oauth/token",
		// 	headers: {"Content-Type" : 'application/x-www-form-urlencoded'},
		// 	data: $.param({
		// 		grant_type: "password",
		// 		username: "admin@cubbyhole.com",
		// 		password: "Supinf0!"
		// 	})
		// }).success(function(data, status, headers, config) {
  //     		APIService.setAccessToken(data.access_token);
  //   	}).error(function(data, status, headers, config) {
  //     		console.log(data);
  //   	});
		// $.ajax({
		// 	type: "POST",
		// 	url: APIService.api_url+"oauth/token",
		// 	headers: {Authorization: "Basic ODM5MjM3OTQ0ODY2MDo5MzA1ZDYyMWYxOWI4OGM2MjllZmU4NTQxMGQxZWIwYg=="},
		// 	crossDomain: false,
		// 	data: {
		// 		grant_type: "password",
		// 		username: "admin@cubbyhole.com",
		// 		password: "Supinf0!"
		// 	}
		// }).done(function(msg){
		// 	console.log(msg);
		// }).error(function(msg) {
		// 	console.log(msg);
		// });
	};

	/**
	 * Fonction qui permet de changer la couleur des incons devant le fields dans formulaire d'inscription et de connexion
	 * @param  {[string]} name  String contenant le name de l'input a modifier
	 * @param  {[string]} color String contenant la couleur a mettre (par default orange)
	 */
	function changeInputIconColor(name, color) {
		color = typeof color !== 'undefined' ? color : "#fc7148";
		$("input[name="+name+"]").prev().css("color", color);
	}

	function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

	function sendError(infos) {
		var span = $("input[name="+infos[0]+"]").next().next();
		if(infos[1] == "required") {
			text = capitaliseFirstLetter(infos[0])+" is required.";
		}
		else if (infos[1] == "wrong"){
			text = capitaliseFirstLetter(infos[0])+" is wrong.";
		}
		else {
			text = "Error";
		}
		span.html(text);
	}

	function capitaliseFirstLetter(string)
	{
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}
}]);