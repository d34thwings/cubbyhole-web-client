var cubbyhole = angular.module('cubbyhole', ['ngRoute', 'ngCookies']);

cubbyhole.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/login', {
        templateUrl: 'pages/login.html'
      }).
      when('/register', {
        templateUrl: 'pages/register.html'
      }).
      when('/reset', {
        templateUrl: 'pages/reset.html'
      }).
      when('/home', {
        templateUrl: 'pages/home.html'
      }).
      otherwise({
        redirectTo: '/login'
      });
}]).run( function($rootScope, $location, $cookieStore) {

    // register listener to watch route changes
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
		if ($cookieStore.get("access_token") == false && $cookies.refresh_token == false) {
	    	if (next.templateUrl != "pages/login.html" && next.templateUrl != "pages/register.html" && next.templateUrl != "pages/reset.html") {
	            $location.path( "/login" );
	        }
        }
    });
 });