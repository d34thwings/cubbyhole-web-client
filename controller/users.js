cubbyhole.controller('UserCtrl', ['$scope', "APIService", "Users", "$http", "$location", function ($scope, APIService, Users, $http, $location) {
	$scope.checkForm = function() {
		var errors = [];

		angular.forEach($scope.register.$error, function(error, index) {
			if(error.length > 0){
				angular.forEach(error, function(info){
					errors.push(info.$name+"_required");
				});
			}
		});

		if($scope.password !== $scope.confirmation && $.inArray("confirmation_required", errors) == -1) {
			errors.push("confirmation_wrong");
		}
		else if($scope.register.password.$error.minlength){
			errors.push("password_length_6");
		}
		else if($scope.register.confirmation.$error.minlength){
			errors.push("confirmation_length_6");
		}
		else{
			resetInput("password");
			resetInput("confirmation");
		}
			
		if(!validateEmail($scope.email) && $.inArray("email_required", errors) == -1){
			errors.push("email_wrong");
		}
		else {
			resetInput("email");
		}
		
		if($scope.lname != "" && $scope.register.lname.$error.minlength) {
			errors.push("lname_length_3");
		}
		else{
			resetInput("lname");
		}
			
		if($scope.fname != "" && $scope.register.fname.$error.minlength) {
			errors.push("fname_length_3");
		}
		else{
			resetInput("fname");
		}
			
		if(errors.length > 0) {
			console.log(errors);
			angular.forEach(errors, function(error) {
				sendError(error);
			});
		}
		else {
			Users.createUser($scope.email,$scope.password,$scope.lname,$scope.fname,function(infos) {
				console.log(infos);
			});
		}
	};
	
	$scope.login = function() {
		if(!$scope.loginForm.$error.required) {
			APIService.authenticateUser($scope.email, $scope.password, function(infos){
				if(infos.access_token){
					$location.path("/home").replace();
					$scope.$apply();
				}
				else{
					if(!validateEmail($scope.email)) {
						sendError("email_wrong");
					}
					else {
						resetInput("email");
					}
					sendError("password_wrong");
				}
			});
		}
		else {
			if(!validateEmail($scope.email)) {
				sendError("email_wrong");
			}
			else {
				resetInput("email");
			}
			angular.forEach($scope.loginForm.$error.required, function(error, index) {
				sendError(error.$name+"_required");
			});
		}
	};
		
	/**
	 * Fonction qui permet de changer la couleur des incons devant le fields dans formulaire d'inscription et de connexion
	 * @param  {[string]} name  String contenant le name de l'input a modifier
	 * @param  {[string]} color String contenant la couleur a mettre (par default orange)
	 */
	function changeInputIconColor(name, color) {
		color = typeof color !== 'undefined' ? color : "#fc7148";
		$("input[name="+name+"]").prev().css("color", color);
	}

	$scope.getEmail = function() {
		return APIService.getEmail();
	}
		
	/**
	 * Fonction qui permet de changer la couleur des incons devant le fields dans formulaire d'inscription pour les rendre comme avant.
	 * @param  {[string]} name  String contenant le name de l'input a modifier
	 */
	function resetInput(name) {
		$("input[name="+name+"]").next().next().attr("class", name+"_error");
	}
		
	/**
	 * Fonction pour valider les adresses mail.
	 * @param  {[string]} email L'adresse email
	 * @return {[bool]}
	 */
	function validateEmail(email) { 
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
		
	/**
	 * Fonction pour envoyer les erreurs en dessous des champs du formulaire
	 * @param  {[array]} infos Tableau avec les informations des input a modifier.
	 */
	function sendError(infos) {
		infos = infos.split("_");
		console.log(infos);
		var span = $("input[name="+infos[0]+"]").next().next();
		if(infos[1] == "required") {
			text = capitaliseFirstLetter(infos[0])+" is required.";
		}
		else if (infos[1] == "wrong"){
			text = capitaliseFirstLetter(infos[0])+" is wrong.";
		}
		else if (infos[1] == "length"){
				text = "This must at least "+infos[2]+" characters.";
		}
		else {
			text = "Error";
		}
		span.html(text);
		span.attr('class', infos[0]+'_error slideLeft');
	}
		
	/**
	 * Fonction pour retourner la chaine avec la premiere lettre en majuscule
	 * @param  {[string]} string La chaine a modifier.
	 * @return {[string]}        La chaine modifiee.
	 */
	function capitaliseFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
}]);