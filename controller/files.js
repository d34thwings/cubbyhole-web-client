cubbyhole.factory('Files', ['$http', "APIService", function ($http, APIService) {
		
	/**
	 * Constructor
	 */
	function Files() {

	};
	/**
	 * Public method
	 */
	Files.prototype = {};


	/**
	 * Static method
	 */
		
	Files.createUser = function(email, password, lname, fname, callback) {
		$.ajax({
			type: "POST",
			url: APIService.getApiUrl()+"users?access_token=",
			data: {
				email: email,
				password: password
			}
		}).done(function(user){
			callback(user);
		});
	};

	Files.getFiles = function(callback) {
		$.ajax({
			type: "GET",
			url: APIService.getApiUrl()+"files?access_token="+APIService.getAccessToken()
		}).done(function(files){
			callback(files.response);
		});
	};

	Files.getUserById = function(id, callback) {
		$.ajax({
			type: "GET",
			url: APIService.getApiUrl()+"users/"+id+"?access_token="+APIService.getAccessToken()
		}).done(function(user){
			callback(user.response);
		});
	};

	Files.getUserByEmail = function(email, callback) {
		$.ajax({
			type: "GET",
			url: APIService.getApiUrl()+"users?access_token="+APIService.getAccessToken()+"&email="+email
		}).done(function(user){
			callback(user.response[0]);
		});
	};

	/**
	 * Private method
	 */
		

	return Files;
}]);