cubbyhole.controller('FoldersCtrl', function ($scope, APIService, Users, $http, $location, Folders) {
	$scope.folders;
	var list = $("#list");

	init = function(){
		Folders.getFolders(function(folders){
			getList(folders);
		});
	};
	init();

	getList = function(infos){
		angular.forEach(infos, function(infos, name){
			if(name == "folders") {
				if(infos.length > 0) {
					angular.forEach(infos, function(folder, index){
						getList(folder);
						addList(1, folder);
					});
				}
			}
			else if(name == "files") {
				if(infos.length > 0) {
					angular.forEach(infos, function(file, index){
						addList(2, file);
					});					
				}
			}
		});
	}

	$scope.create = function(){
		var name = prompt("Folder's name ?");
		Folders.createFolder(name, null, function(folder){
			if(folder.response){
				alert("Your folder is created");
				addList(1, folder.response);
			}
			else {
				console.log(folder);
				alert("There is a problem, please try again.");
			}
		})
	}

	$scope.delete = function(){
		var checkboxs = $("#list input[type=checkbox]");
		for (var i = 0; i < checkboxs.length; i++) {
			if(checkboxs[i].checked) {
				Folders.deleteFolder(checkboxs[i].value, function(infos){
					console.log(infos);
				})
			}
		};
	}

	$scope.rename = function(){
		var checkboxs = $("#list input[type=checkbox]");
		for (var i = 0; i < checkboxs.length; i++) {
			if(checkboxs[i].checked) {
				var folderId = checkboxs[i].value;
				var name = $("#"+folderId).next().text();
				var newName = prompt("New folder's name ?", name);
				Folders.updateFolder(folderId, newName, null, function(folder){
					console.log(folder);
					if(folder.response) {
						$("#"+folderId).next().text(newName)
					}
				})
			}
		};
	}

	addList = function(status, folder){
		var html = $("#list").html();
		html += newLine(status, folder);
		$("#list").html(html);
	}

	newLine = function(status, infos) {
		if(status == 1) {
			return "<tr><td id="+infos._id+"><input type='checkbox' value='"+infos._id+"'/></td><td><span class='glyphicon glyphicon-folder-open'></span>"+infos.foldername+"</td><td>0 MO</td><td>"+infos.date+"</td></tr>";
		}
		else if (status == 2) {

		}
	}
	
});