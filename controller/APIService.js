cubbyhole.factory('APIService', ['$cookies', '$cookieStore', function ($cookies, $cookieStore) {
	var api_url = "http://api-cubbyhole.rhcloud.com/";
	return {
		getApiUrl: function (){
			return api_url;
		},
		getEmail: function (){
			return $cookieStore.get("email");
		},
		getAccessToken: function (){
			if($cookieStore.get("access_token")) {
				return $cookieStore.get("access_token");				
			}
			else {
				return false;
			}
		},
		setAccessToken: function (string){
			$cookieStore.put("access_token", string);
		},
		authenticateUser: function(email, password, callback) {
			$.ajax({
				type: "POST",
				url: api_url+"oauth/token",
				headers: {
					Authorization: "Basic ODM5MjM3OTQ0ODY2MDo5MzA1ZDYyMWYxOWI4OGM2MjllZmU4NTQxMGQxZWIwYg=="
				},
				data: {
					grant_type: "password",
					username: email,
					password: password
				}
			}).done(function(oauth){
				$cookieStore.put("access_token", oauth.access_token);
				$cookieStore.put("email", email);
				$cookies.refresh_token = oauth.refresh_token;
				callback(oauth);
			}).error(function(error) {
				callback(error);
			});
		},
		getRefreshToken: function() {
			if($cookies.refresh_token) {
				return $cookies.refresh_token;
			}
			else {
				return false;
			}
		},
		checkAccessToken: function(callback) {
			$.ajax({
				type: "GET",
				url: api_url+"users?access_token="+$cookieStore.get("access_token")
			}).done(function(infos){
				if(infos.status == 401) {
					callback(false);
				}
				else if (infos.response) {
					callback(true);
				}
			});
		},
		updateAccessToken: function(callback) {
			$.ajax({
				type: "POST",
				url: api_url+"oauth/token",
				headers: {Authorization: "Basic ODM5MjM3OTQ0ODY2MDo5MzA1ZDYyMWYxOWI4OGM2MjllZmU4NTQxMGQxZWIwYg=="},
				data: {
					grant_type: "refresh_token",
					refresh_token: $cookies.refresh_token
				}
			}).done(function(oauth){
				$cookieStore.put("access_token", oauth.access_token);
				$cookies.refresh_token = oauth.refresh_token;
				callback();
			});
		},
		isAuthenticate: function(callback){
			this.checkAccessToken(function(check){
				if(check){
					callback(0);
				}
				else if($cookies.refresh_token) {
					callback(1);
				}
				else {
					window.location = "/login";
				}
			})
		}
	};
}]);