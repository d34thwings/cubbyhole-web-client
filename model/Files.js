cubbyhole.controller('FilesCtrl', function ($scope, APIService, Users, $http, $location, Files) {
	init = function(callback){
		Files.getFiles(function(fichiers){
			callback(getList(fichiers));
		});
	}
		$scope.html = "";
	getList = function(list){
		angular.forEach(list, function(type, name){
			if(type != "fichier"){
				$scope.html += "<div>";
				$scope.html += getList(type);				
				$scope.html += "</div>";
			}
			else {
				$scope.html += "<span>"+name+"</span>";
			}
		});
		return $scope.html;
	}
		init(function(files){
		$scope.files = files;
	});
});