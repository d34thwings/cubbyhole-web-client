cubbyhole.factory('Users', ['$http', "APIService", function ($http, APIService) {
		
	/**
	 * Constructor
	 */
	function Users() {
		
	};

	/**
	 * Public method
	 */
	Users.prototype = {};


	/**
	 * Static method
	 */
		
	Users.createUser = function(email, password, lname, fname, callback) {
		$.ajax({
			type: "POST",
			url: APIService.getApiUrl()+"users?access_token=",
			data: {
				email: email,
				password: password
			}
		}).done(function(user){
			callback(user);
		});
	};

	Users.setData = function(email){
		this.email = email;
	};

	Users.getUsers = function(callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				$.ajax({
					type: "GET",
					url: APIService.getApiUrl()+"users?access_token="+APIService.getAccessToken()
				}).done(function(users){
					callback(users.response);
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					$.ajax({
						type: "GET",
						url: APIService.getApiUrl()+"users?access_token="+APIService.getAccessToken()
					}).done(function(users){
						callback(users.response);
					});
				});
			}
		});
	};

	Users.getUserById = function(id, callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				$.ajax({
					type: "GET",
					url: APIService.getApiUrl()+"users/"+id+"?access_token="+APIService.getAccessToken()
				}).done(function(user){
					callback(user.response);
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					$.ajax({
						type: "GET",
						url: APIService.getApiUrl()+"users/"+id+"?access_token="+APIService.getAccessToken()
					}).done(function(user){
						callback(user.response);
					});
				});
			}
		});
	};

	Users.getUserByEmail = function(email, callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				$.ajax({
					type: "GET",
					url: APIService.getApiUrl()+"users?access_token="+APIService.getAccessToken()+"&email="+email
				}).done(function(user){
					callback(user.response[0]);
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					$.ajax({
						type: "GET",
						url: APIService.getApiUrl()+"users?access_token="+APIService.getAccessToken()+"&email="+email
					}).done(function(user){
						callback(user.response[0]);
					});
				});
			}
		});
	};

	/**
	 * Private method
	 */
	

	return Users;
}]);