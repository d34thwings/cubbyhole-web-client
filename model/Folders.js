cubbyhole.factory('Folders', ['$http', "APIService", "Users", function ($http, APIService, Users) {
		
	/**
	 * Constructor
	 */
	function Folders() {

	};

	/**
	 * Public method
	 */
	Folders.prototype = {};


	/**
	 * Static method
	 */
		
	Folders.createFolder = function(foldername, parent, callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				Users.getUserByEmail(APIService.getEmail(), function(user){
					$.ajax({
						type: "POST",
						url: APIService.getApiUrl()+"folders?access_token="+APIService.getAccessToken(),
						data: {
							folder_name: foldername,
							_parent: parent,
							_owner: user._id
						}
					}).done(function(folder){
						callback(folder);
					});			
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					Users.getUserByEmail(APIService.getEmail(), function(user){
						$.ajax({
							type: "POST",
							url: APIService.getApiUrl()+"folders?access_token="+APIService.getAccessToken(),
							data: {
								folder_name: foldername,
								_parent: parent,
								_owner: user._id
							}
						}).done(function(folder){
							callback(folder);
						});			
					});
				});
			}
		});
	};
		
	Folders.updateFolder = function(id, foldername, parent, callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				Users.getUserByEmail(APIService.getEmail(), function(user){
					$.ajax({
						type: "PUT",
						url: APIService.getApiUrl()+"folders/"+id+"?access_token="+APIService.getAccessToken(),
						data: {
							foldername: foldername,
							_parent: parent,
							_owner: user._id
						}
					}).done(function(folder){
						callback(folder);
					});			
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					Users.getUserByEmail(APIService.getEmail(), function(user){
						$.ajax({
							type: "PUT",
							url: APIService.getApiUrl()+"folders/"+id+"?access_token="+APIService.getAccessToken(),
							data: {
								foldername: foldername,
								_parent: parent,
								_owner: user._id
							}
						}).done(function(folder){
							callback(folder);
						});			
					});
				});
			}
		});
	};

	Folders.deleteFolder = function(id, callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				$.ajax({
					type: "DELETE",
					url: APIService.getApiUrl()+"folders/"+id+"?access_token="+APIService.getAccessToken()
				}).done(function(folders){
					callback(folders);
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					$.ajax({
						type: "DELETE",
						url: APIService.getApiUrl()+"folders/"+id+"?access_token="+APIService.getAccessToken()
					}).done(function(folders){
						callback(folders);
					});
				});
			}
		});
	};

	Folders.getFolders = function(callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				$.ajax({
					type: "GET",
					url: APIService.getApiUrl()+"hierarchy?access_token="+APIService.getAccessToken()
				}).done(function(folders){
					callback(folders.response);
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					$.ajax({
						type: "GET",
						url: APIService.getApiUrl()+"hierarchy?access_token="+APIService.getAccessToken()
					}).done(function(folders){
						callback(folders.response);
					});
				});
			}
		})
	};

	Folders.getFoldersById = function(id, callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				$.ajax({
					type: "GET",
					url: APIService.getApiUrl()+"folders/"+id+"?access_token="+APIService.getAccessToken()
				}).done(function(folder){
					callback(folder.response);
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					$.ajax({
						type: "GET",
						url: APIService.getApiUrl()+"folders/"+id+"?access_token="+APIService.getAccessToken()
					}).done(function(folder){
						callback(folder.response);
					});
				});
			}
		})
	};

	Folders.getFoldersByUser = function(email, callback) {
		APIService.isAuthenticate(function(isLogIn){
			if(isLogIn == 0){
				Users.getUserByEmail(APIService.getEmail(), function(user){
					$.ajax({
						type: "GET",
						url: APIService.getApiUrl()+"folders?access_token="+APIService.getAccessToken()+"&_owner="+user._id
					}).done(function(folders){
						callback(folders.response);
					});
				});
			}
			else if (isLogIn == 1) {
				APIService.updateAccessToken(function(){
					Users.getUserByEmail(APIService.getEmail(), function(user){
						$.ajax({
							type: "GET",
							url: APIService.getApiUrl()+"folders?access_token="+APIService.getAccessToken()+"&_owner="+user._id
						}).done(function(folders){
							callback(folders.response);
						});
					});
				});
			}
		})
	};

	/**
	 * Private method
	 */
		

	return Folders;
}]);